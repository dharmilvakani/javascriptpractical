const calculator = document.getElementById("calculator");
let memory = 0;
function deleteValue(calculator) {
    length = calculator.display.value.length;
    calculator.display.value = calculator.display.value.substring(0, length - 1);
}

function pi(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.PI * x;
}

function square(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.pow(x, 2);
}

function reciprocal(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.pow(x, -1)
}

function mod(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.abs(x);
}

function exp(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.exp(x);
}

function sqrt(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.sqrt(x);
}

function factorial(calculator) {
    fact = 1;
    x = calculator.display.value;
    for (let i = 1; i <= x; i++) {
        fact = fact * i;
        calculator.display.value = fact;
    }
}

function powerOf10(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.pow(10, x);
}

function log(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.log10(x)
}

function ln(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.log(x)
}

function result(calculator) {
    calculator.display.value = eval(calculator.display.value);
}

function F_E(calculator) {
    x = calculator.display.value;
    calculator.display.value = parseFloat(x).toExponential(10)
}

function degtorad(calculator) {
    calculator.display.value = calculator.display.value * (Math.PI / 180);
}

function MemoryClear(calculator) {
    x = calculator.display.value;
    calculator.display.value = ' ';
}

function MemoryRecall(calculator) {
    x = calculator.display.value;
    calculator.display.value = memory;
}

function MemoryStore(calculator) {
    x = calculator.display.value;
    memory = x;
    calculator.display.value = memory;
}

function MemoryPlus(calculator) {
    x = calculator.display.value;
    memory = eval(`${memory} + ${x}`)
}

function MemoryMinus(calculator) {
    x = calculator.display.value;
    memory = eval(`${memory} - ${x}`)
}


function sin(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.sin(x);

}
function cos(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.cos(x);

}
function tan(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.tan(x);
}

function abs(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.abs(x);
}

function asinh(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.asinh(x);
}
function acosh(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.acosh(x);
}

function atanh(calculator) {
    x = calculator.display.value;
    calculator.display.value = Math.atanh(x);
}

